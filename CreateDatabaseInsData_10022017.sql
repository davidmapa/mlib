
/** Creating a local database
Create database scdmapa_dev;

CREATE USER 'dmapa'@'localhost' IDENTIFIED BY 'mapa5986';
GRANT ALL PRIVILEGES ON * . * TO 'dmapa'@'localhost';

**/

use scdmapa_dev;

create table mlib_types (
  id int unsigned not null auto_increment primary key,
  type tinytext,
  status tinytext);
  
create table mlib_users (
  id int unsigned not null auto_increment primary key,
  first tinytext,
  last tinytext,
  email tinytext);    
  
create table media (
  id int unsigned not null auto_increment primary key,
  title tinytext,
  author tinytext,
  description tinytext,
  type tinytext, 
  user_id int unsigned,
  status tinytext,
  date_in date);    

Insert into mlib_types values (
     null,"book","active");
Insert into mlib_types values (
     null,"DVD","active");
     
Insert into mlib_users values (
     null,"Neil","Armstrong","thefirst@nasa.gov");
Insert into mlib_users values (
     null,"David","Mapa","David.Mapa_a2z@yahoo.com"); 
     
Insert into media values (
     null,
	 "The Moon is a Harsh Mistress",
	 "book",
	 "Robert Heinlein",
	 "Computer helps convicts to freedom on the moon.",
	 1,
	 "active",
	 "2014-09-30");  







	 
