<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("mlib status");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

# Code for your web page follows.
try
{
  //open the database
  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
?>

<h2>Media Status</h2>
<!-- display all equipment -->
<table border=1>
  <tr bgcolor="#cccccc">
    <td align="center"><b>Title</b></td>
	<td align="center"><b>Author</b></td>
	<td align="center"><b>Description</b></td>
	<td align="center"><b>Type</b></td>
	<td align="center"><b>User</b></td>
	<td align="center"><b>Status</b></td>
	<td align="center"><b>Date</b></td>
  </tr>

  <?php
    
  $sql = "SELECT * FROM media WHERE status = 'active'";
  $result = $db->query($sql);
  foreach($result as $row) {
    print "<tr>";
    print "<td>".$row['title']."</td>";
    print "<td>".$row['author']."</td>";
    print "<td>".$row['description']."</td>";
	print "<td>".$row['type']."</td>";
	$user_id = $row['user_id'];
	if ($user_id > 0) {
		$result = $db->query("SELECT * FROM mlib_users where id = $user_id")->fetch();
		$user_name = $result['first']." ".$result['last'];
		$date_in = $row['date_in'];
	} else {
		$user_name = "available";
		$date_in = "not reserved";
	}
	
    print "<td>".$user_name."</td>";	
	print "<td>".$row['status']."</td>";
	print "<td>".$date_in."</td>";
    print "</tr>";
  }

  print "</table>";

  // close the database connection
  $db = NULL;
}
catch(PDOException $e)
{
  echo 'Exception : '.$e->getMessage();
  echo "<br/>";
  $db = NULL;
}
require('mlib_footer.php');
?>
